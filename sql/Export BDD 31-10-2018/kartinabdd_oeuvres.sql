-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: kartinabdd
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oeuvres`
--

DROP TABLE IF EXISTS `oeuvres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oeuvres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_orientation` int(11) DEFAULT NULL,
  `prix_de_base` float DEFAULT NULL,
  `id_artiste` int(11) DEFAULT NULL,
  `titre_photo` varchar(255) DEFAULT NULL,
  `lien_image` varchar(1000) DEFAULT NULL,
  `nb_tirages` int(11) DEFAULT NULL,
  `nb_tirages_restants` int(11) DEFAULT NULL,
  `date_mise_en_vente` datetime DEFAULT NULL,
  `date_fin_de_vente` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orientation_oeuvre_idx` (`id_orientation`),
  KEY `artiste_oeuvre_idx` (`id_artiste`),
  CONSTRAINT `artiste_oeuvre` FOREIGN KEY (`id_artiste`) REFERENCES `utilisateurs` (`id`),
  CONSTRAINT `orientation_oeuvre` FOREIGN KEY (`id_orientation`) REFERENCES `orientations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oeuvres`
--

LOCK TABLES `oeuvres` WRITE;
/*!40000 ALTER TABLE `oeuvres` DISABLE KEYS */;
INSERT INTO `oeuvres` VALUES (1,2,80,4,'Pinnacles','australie.jpg',1000,1000,'2018-09-21 12:05:28',NULL),(2,2,60,5,'Peluche','black_white.jpg',500,490,'2018-09-21 12:06:38',NULL),(3,2,75,6,'Café','cafe.jpg',2000,190,'2018-09-21 12:08:07',NULL),(4,2,89,7,'Camping Car','campingcar.jpg',400,400,'2018-09-21 12:09:34',NULL),(5,4,70,7,'Cascade','cascade.jpg',850,850,'2018-09-21 12:10:39',NULL),(6,2,60,4,'Chat','chat.jpg',400,0,'2018-09-21 12:11:29','2018-10-10 09:23:16'),(7,2,78,6,'Chevaux','chevaux.jpg',500,393,'2018-09-21 12:12:27',NULL),(8,4,45,8,'Chevre','chevre.jpg',200,200,'2018-09-21 12:13:33',NULL),(9,2,89,9,'Cocktail','cocktail.jpg',450,43,'2018-09-21 12:14:13',NULL),(10,2,78,8,'Concert','concert.jpg',500,490,'2018-09-21 12:31:48',NULL),(11,2,70,7,'Cross Fit','crossfit.jpg',1500,1500,'2018-09-21 12:31:48',NULL),(12,4,100,6,'Tour Eiffel','eiffel.jpg',1000,800,'2018-09-21 12:31:48',NULL),(13,1,48,5,'Elephant','elephant.jpg',780,772,'2018-09-21 12:31:48',NULL),(14,1,70,4,'Eleve','eleve.jpg',700,700,'2018-09-21 12:31:48',NULL),(15,3,120,8,'Mont Everest','everest.jpg',800,799,'2018-09-21 12:31:48',NULL),(16,1,100,7,'Fruits des bois','fruits.jpg',789,789,'2018-09-21 12:31:48',NULL),(17,2,45,6,'Mont Fuji','fuji.jpg',100,50,'2018-09-21 12:31:48',NULL),(18,2,80,5,'Future star','garcon.jpg',450,450,'2018-09-21 12:31:48',NULL),(19,2,74,4,'Gauffres','gauffres.jpg',300,300,'2018-09-21 12:31:48',NULL),(20,2,50,6,'Glace','glace.jpg',200,200,'2018-09-21 12:31:48',NULL),(21,2,48,7,'Grèce','grece.jpg',899,899,'2018-09-21 12:31:48',NULL),(22,2,88,8,'Guitariste','guitare.jpg',800,800,'2018-09-21 12:31:48',NULL),(23,2,65,9,'Guitariste','guitariste.jpg',50,50,'2018-09-21 12:31:48',NULL),(24,1,40,5,'Huile d\'olive','huile.jpg',400,400,'2018-09-21 12:31:48',NULL),(25,2,88,6,'Kangourou','kangourou.jpg',560,560,'2018-09-21 12:31:48',NULL),(26,2,110,7,'Karate','karate.jpg',600,600,'2018-09-21 12:31:48',NULL),(27,2,120,8,'Karijini','karijini.jpg',500,500,'2018-09-21 12:31:48',NULL),(28,2,85,9,'Lac','lac.jpg',400,400,'2018-09-21 12:31:48',NULL),(29,1,100,4,'Lion','lion.jpg',500,500,'2018-09-21 12:31:48',NULL),(30,2,45,4,'Londres','londres.jpg',400,400,'2018-09-21 12:31:48',NULL),(31,4,88,6,'Portrait femme','lunettes.jpg',489,489,'2018-09-21 12:31:48',NULL),(32,3,150,5,'Montagnes','montagnes.jpg',550,550,'2018-09-21 12:31:48',NULL),(33,2,100,6,'Montgolfière','montgolfiere.jpg',400,400,'2018-09-21 12:31:48',NULL),(34,2,58,8,'Canöe','mountain.jpg',459,459,'2018-09-21 12:31:48',NULL),(35,3,88,9,'Panorama Lac','panorama.jpg',600,600,'2018-09-21 12:31:48',NULL),(36,2,70,7,'Perroquet','perroquet.jpg',500,500,'2018-09-21 12:31:48',NULL),(37,2,45,8,'Appareil photo','photo.jpg',400,400,'2018-09-21 12:31:48',NULL),(38,2,60,9,'Riz','riz.jpg',300,300,'2018-09-21 12:31:48',NULL),(39,2,75,6,'Running','running.jpg',250,250,'2018-09-21 12:36:39',NULL),(41,2,60,8,'Lever du soleil','sunrise.jpg',500,500,'2018-09-21 12:36:39',NULL),(42,2,80,9,'Open d\'Australie','tennis.jpg',489,489,'2018-09-21 12:36:39',NULL),(43,2,85,8,'Temple Thaïlande','thailande.jpg',500,500,'2018-09-21 12:36:39',NULL),(44,4,60,7,'Toile d\'araignée','toile.jpg',450,450,'2018-09-21 12:36:39',NULL),(45,2,70,8,'Tortue','tortue.jpg',560,560,'2018-09-21 12:36:39',NULL),(46,3,66,4,'Panorama Ville','ville.jpg',600,600,'2018-09-21 12:36:39',NULL),(47,2,40,5,'Violons','violons.jpg',280,278,'2018-10-09 15:07:35',NULL),(50,2,46,4,'Athletisme','photo50.jpg',500,498,'2018-10-09 15:16:13',NULL),(51,2,55,4,'Sunset','sunset.jpg',120,119,'2018-10-09 15:21:56',NULL),(52,2,80,4,'Maldives','photo52.jpg',120,118,'2018-10-12 12:02:12',NULL),(53,2,36,6,'Oiseau','photo53.jpg',700,698,'2018-10-24 14:53:05',NULL),(54,4,77,6,'Tigrou','photo54.jpg',250,250,'2018-10-24 15:28:57',NULL),(55,2,66,18,'Bouddhisme','photo55.jpg',450,449,'2018-10-25 16:17:45',NULL),(56,2,78,18,'Plage','photo56.jpg',777,777,'2018-10-31 15:15:22',NULL),(57,2,320,6,'Léopard','photo57.jpg',677,677,'2018-10-31 16:04:29',NULL),(58,2,67,6,'Chouette','photo58.jpg',123,123,'2018-10-31 16:09:36',NULL);
/*!40000 ALTER TABLE `oeuvres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 16:39:02
