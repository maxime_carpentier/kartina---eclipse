-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: kartinabdd
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `nom` varchar(45) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `rue` varchar(255) NOT NULL,
  `codePostal` varchar(45) NOT NULL,
  `ville` varchar(45) NOT NULL,
  `pays` varchar(45) NOT NULL,
  `compteBloque` tinyint(4) NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `artiste` tinyint(4) NOT NULL DEFAULT '0',
  `biographie` varchar(1000) DEFAULT NULL,
  `email` varchar(155) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateurs`
--

LOCK TABLES `utilisateurs` WRITE;
/*!40000 ALTER TABLE `utilisateurs` DISABLE KEYS */;
INSERT INTO `utilisateurs` VALUES (1,'M','Maxime','Carpentier','0784985877','276 rue du Plumont','59242','Genech','France',0,1,0,NULL,'maxime2.carpentier@hotmail.com'),(2,'M','Cyriaque','Mahiette','0798487988','48 rue de Lille','59551','Attiches','Fra',0,0,0,NULL,'cyriaque.mahiette@hotmail.fr'),(3,'M','Patrick','Raemdonck','0648988898','432 rue de Tourcoing','59223','Roncq','Fra',1,0,0,NULL,'patrick.raemdonck@hotmail.fr'),(4,'M','Aurélien','Villette','0748788987','48 rue Faidherbe','59000','Lille','France',0,0,1,'Au-delà  d\'un simple souci documentaire et de pérennisation, Aurélien Villette s\'intéresse à  la description et à  l\'esthétisation de lieux en déshérence. Les jeux de lumières, les matières vieillissantes, les perspectives et structures architecturales sont mis en évidence et magnifiés. Le silence, omniprésent, paraît correspondre à  un temps qui se serait tu et fait émaner de ces photographies une atmosphère énigmatique, questionnant le spectateur sur la vie passée et l\'esprit de ces lieux... !!!','aurelien.villette@hotmail.fr'),(5,'M','Laurent','Dequick','0748889966','789 rue Moliere','75000','Paris','France',0,0,1,'Ce photographe, âgé de 40 ans, est architecte de formation. Son travail s\'en trouve marqué puisqu\'il s\'agit en premier lieu d\'une réflexion sur la ville contemporaine et plus spécifiquement sur le foisonnement de l\'espace urbain moderne. Le propos de Laurent Dequick est de transmettre avec exactitude l\'impression de frénésie qui découle de la densité de population et de l\'activité en zone urbaine : « Au détour des rues, les lumières, les bruits, le trafic des voitures, le fourmillement des piétons, le mélange des odeurs, sont si prenants qu\'aucune prise de vue unique ne peut en capter l\'intégralité. Faudrait-il donc faire des choix ? Je ne le crois pas, je ne le veux pas? » ','laurent.dequick@hotmail.fr'),(6,'Mme','Nolwenn','Hadet','0987412565','58 rue de Washington','10001','New-York','USA',0,0,1,'Née en 1973 à Vannes dans le Morbihan et menant une brillante carrière juridique entre Nantes et Paris, Nolwenn Hadet quitte rarement sa Bretagne natale, hormis qu\'elle entreprend des expéditions exceptionnelles afin de satisfaire sa passion incommensurable pour la photographie. Elle réalise ses premières images dès son plus jeune âge et reçoit en cadeau à 7 ans un appareil argentique. Fidèle compagnon de voyage, son Agfamatic la suivra partout et sera le témoin de ses perfectionnements techniques au fil du temps. Les images de Nolwenn Hadet sont donc indissociablement liées à ses voyages, aux paysages qu\'elle parcoure, aux hommes qu\'elle rencontre et aux animaux qu\'elle croise dans les régions les plus reculées du monde. Des vastes plaines du Botswana en Afrique australe aux surfaces glacées de la banquise dans l\'Arctique, la planète est bel et bien son terrain de jeux. ','nolwenn.hadet@hotmail.fr'),(7,'M','Damien','Dufresne','0748899956','78 rue albert','1000','Bruxelles','Belgique',0,0,1,'Damien Dufresne est un photographe atypique: reconnu dans les domaines du maquillage et de la publicité, il trouve dans son parcours de photographe d’art la liberté d’oublier les codes et les contraintes de la mode. …« La photographie d’art me donne une vraie liberté, je peux laisser libre cours à mon imagination, je ne cherche pas à provoquer mais je ne m’interdis rien, je suis mon instinct, je fais des images qui ont un sens pour moi, des images qui me parlent …» … « Pour moi, dessiner, peindre ou photographier est un moyen d’expression, de raconter des histoires sans utiliser les mots, une forme de langage, une façon différente de communiquer »… … « La technique n’est pas une fin mais un moyen qui doit servir la créativité… l’émotion est ce qui compte le plus pour moi »…. Sa force: la créativité, sa spécificité: les textures et les couleurs, son moteur: la passion.','damien.dufresne@hotmail.fr'),(8,'Mme','Mathilde','Oscar','0898788895','563 rue de l\'océan','1000001','Tokyo','Japon',0,0,1,'Suite à ses études en histoire de l’art dont elle fait constamment référence, l’univers onirique de la jeune photographe Mathilde Oscar s’articule autour de mythes et de légendes qu’elle revisite dans un contexte actuel. Ses portraits sont davantage picturaux que photographiques comme en témoigne la « Jeune fille à la fraise » faisant référence au chef d’œuvre de 1665 « La jeune fille à la perle » du peintre Vermeer et qui caractérise l’âge d’or de la peinture néerlandaise. ','mathilde.oscar@hotmail.fr'),(9,'M','Andy','Yeung','0645889899','41 rue de Hong-Kong','065001','Pékin','Chine',0,0,1,'D\'origine hongkongaise, Andy Yeung est un jeune artiste maintes fois primé dont l\'intérêt artistique se porte essentiellement sur la photographie d\'architecture, de paysage et de voyage. Celui pour qui le précepte est de « toujours regarder en l\'air » ne cesse de capturer des immeubles et des constructions modernes depuis l\'obtention de son premier appareil en 2005. Il acquiert très rapidement des bases solides en technique photographique, et depuis la mégalopole de Hong Kong où il vit, il voyage de part et d\'autre du monde à la recherche d\'inspiration.','andy.yeung@hotmail.fr'),(17,'M','MBappe','Kylian','0647584752','10 rue de Paris','75000','Paris','France',1,0,0,NULL,'kmbp@hotmail.fr'),(18,'M','Luis','Aguilera','0787654322','45 rue de elvis','54333','LOS ANGELES','USA',0,0,1,'D’origine vénézuélienne, Luis Aguilera s’installe à Miami pendant son adolescence afin de poursuivre des études artistiques à la Florida International University. Photographe et cinéaste talentueux, il s’intéresse aux nouvelles technologies et survole des paysages à 200 mètres de hauteur pour réaliser ses clichés des bords de mer surpeuplés de Floride et d’Hawaii. La série aérienne « Thalassophile » dont est tirée cette photographie met l\'accent sur la petitesse de l\'homme face aux éléments naturels et notamment, face à l’immensité et à la puissance des océans.','luis.aguilera@hotmail.fr');
/*!40000 ALTER TABLE `utilisateurs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 16:39:05
