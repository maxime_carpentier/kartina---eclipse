-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: kartinabdd
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `themes_oeuvres`
--

DROP TABLE IF EXISTS `themes_oeuvres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `themes_oeuvres` (
  `id_theme` int(11) NOT NULL,
  `id_oeuvre` int(11) NOT NULL,
  KEY `theme_idx` (`id_theme`),
  KEY `oeuvre_idx` (`id_oeuvre`),
  CONSTRAINT `oeuvre` FOREIGN KEY (`id_oeuvre`) REFERENCES `oeuvres` (`id`),
  CONSTRAINT `theme` FOREIGN KEY (`id_theme`) REFERENCES `themes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes_oeuvres`
--

LOCK TABLES `themes_oeuvres` WRITE;
/*!40000 ALTER TABLE `themes_oeuvres` DISABLE KEYS */;
INSERT INTO `themes_oeuvres` VALUES (1,2),(1,6),(1,11),(1,12),(1,14),(1,18),(1,28),(1,31),(1,44),(2,1),(2,4),(2,5),(2,9),(2,12),(2,15),(2,17),(2,20),(2,21),(2,23),(2,27),(2,28),(2,30),(2,32),(2,33),(2,34),(2,35),(2,37),(2,41),(2,43),(2,46),(3,3),(3,9),(3,16),(3,19),(3,20),(3,24),(3,38),(4,11),(4,26),(4,34),(4,39),(4,42),(5,10),(5,18),(5,22),(5,23),(5,23),(5,47),(6,6),(6,7),(6,8),(6,13),(6,25),(6,29),(6,36),(6,45),(1,50),(4,50),(2,51),(2,52),(6,53),(6,54),(2,55),(2,56),(2,56),(1,57),(6,57),(6,58);
/*!40000 ALTER TABLE `themes_oeuvres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 16:39:02
