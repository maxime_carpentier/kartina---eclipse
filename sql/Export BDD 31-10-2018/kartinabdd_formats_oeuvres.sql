-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: kartinabdd
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `formats_oeuvres`
--

DROP TABLE IF EXISTS `formats_oeuvres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `formats_oeuvres` (
  `id_format` int(11) NOT NULL,
  `id_oeuvre` int(11) NOT NULL,
  KEY `format_idx` (`id_format`),
  KEY `oeuvre_format_idx` (`id_oeuvre`),
  CONSTRAINT `format` FOREIGN KEY (`id_format`) REFERENCES `formats` (`id`),
  CONSTRAINT `oeuvre_format` FOREIGN KEY (`id_oeuvre`) REFERENCES `oeuvres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formats_oeuvres`
--

LOCK TABLES `formats_oeuvres` WRITE;
/*!40000 ALTER TABLE `formats_oeuvres` DISABLE KEYS */;
INSERT INTO `formats_oeuvres` VALUES (1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,41),(1,42),(1,43),(1,44),(1,45),(1,46),(1,1),(2,1),(2,5),(2,9),(2,11),(2,11),(2,14),(2,17),(2,22),(2,23),(2,24),(2,28),(2,30),(2,31),(2,37),(2,39),(2,41),(2,43),(2,44),(3,2),(3,8),(3,10),(3,13),(3,17),(3,20),(3,22),(3,25),(3,27),(3,29),(3,31),(3,34),(3,36),(3,37),(3,39),(3,41),(3,42),(3,45),(3,46),(4,2),(4,3),(4,5),(4,7),(4,9),(4,11),(4,14),(4,17),(4,19),(4,21),(4,24),(4,29),(4,30),(4,34),(4,35),(4,38),(4,41),(4,43),(4,45),(1,47),(2,47),(1,50),(4,50),(1,51),(2,51),(1,52),(3,52),(1,53),(2,53),(1,54),(3,54),(4,54),(1,55),(4,55),(3,56),(2,56),(1,56),(1,57),(2,57),(4,57),(1,58),(2,58);
/*!40000 ALTER TABLE `formats_oeuvres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 16:39:04
