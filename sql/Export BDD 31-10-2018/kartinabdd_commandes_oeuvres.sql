-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: kartinabdd
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `commandes_oeuvres`
--

DROP TABLE IF EXISTS `commandes_oeuvres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `commandes_oeuvres` (
  `id_commande` int(11) NOT NULL,
  `id_oeuvre` int(11) NOT NULL,
  `id_cadre` int(11) NOT NULL,
  `id_finition` int(11) NOT NULL,
  `id_format` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `prix` float NOT NULL,
  KEY `commande_oeuvre_idx` (`id_commande`),
  KEY `oeuvre_commande_idx` (`id_oeuvre`),
  KEY `cadre_idx` (`id_cadre`),
  KEY `finition_idx` (`id_finition`),
  KEY `format_idx_comm` (`id_format`),
  CONSTRAINT `cadre` FOREIGN KEY (`id_cadre`) REFERENCES `cadre` (`id`),
  CONSTRAINT `commande_oeuvre` FOREIGN KEY (`id_commande`) REFERENCES `commandes` (`id`),
  CONSTRAINT `finition` FOREIGN KEY (`id_finition`) REFERENCES `finition` (`id`),
  CONSTRAINT `format_commande` FOREIGN KEY (`id_format`) REFERENCES `formats` (`id`),
  CONSTRAINT `oeuvre_commande` FOREIGN KEY (`id_oeuvre`) REFERENCES `oeuvres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commandes_oeuvres`
--

LOCK TABLES `commandes_oeuvres` WRITE;
/*!40000 ALTER TABLE `commandes_oeuvres` DISABLE KEYS */;
INSERT INTO `commandes_oeuvres` VALUES (1,4,7,4,1,1,115.7),(1,5,6,3,2,1,182),(2,13,5,1,3,2,1881.98),(2,16,9,5,1,1,182),(3,23,2,2,2,2,1641.83),(4,34,3,2,3,1,1465.02),(15,7,4,1,4,1,3822.78),(15,13,2,2,3,1,1212.43),(23,52,1,3,3,1,416),(24,51,1,3,2,3,143),(25,47,1,3,2,1,104),(26,53,1,3,3,1,187.2),(26,50,2,2,3,1,1161.91),(27,53,9,5,1,1,65.52),(28,13,1,3,3,1,249.6),(29,47,1,3,2,1,104),(30,55,2,2,4,1,4167.73),(31,15,7,4,1,2,156),(31,52,3,2,3,1,2020.72),(32,50,9,4,1,1,59.8);
/*!40000 ALTER TABLE `commandes_oeuvres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 16:39:03
