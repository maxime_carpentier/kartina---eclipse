package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/ModifierUtilisateur")
public class ModifierUtilisateur extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public ModifierUtilisateur() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		String body = request.getReader().lines().collect(Collectors.joining());
		Client c = new Gson().fromJson(body, Client.class);
		Adresse a = new Gson().fromJson(body, Adresse.class);
		
		int id = Integer.parseInt(request.getParameter("id"));

		try {
			ConnexionBD connexionBD = new ConnexionBD();

			PreparedStatement ps = connexionBD.getCnx().prepareStatement(
					"UPDATE kartinabdd.utilisateurs SET civilite=?, nom=?, prenom=?, telephone=?, rue=?, codePostal=?, ville=?, pays=? where id=" + id);
			ps.setString(1, c.getCivilite());
			ps.setString(2, c.getNom());
			ps.setString(3, c.getPrenom());
			ps.setString(4, c.getTelephone());
			ps.setString(5, a.getRue());
			ps.setString(6, a.getCodePostal());
			ps.setString(7, a.getVille());
			ps.setString(8, a.getPays());
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
