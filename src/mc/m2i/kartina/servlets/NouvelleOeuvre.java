package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.enumerations.Theme;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/NouvelleOeuvre")
public class NouvelleOeuvre extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public NouvelleOeuvre() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id_artiste = Integer.parseInt(request.getParameter("id_artiste"));
		try {
			request.setCharacterEncoding("UTF-8");
			ConnexionBD connexionBD = new ConnexionBD();
			String body = request.getReader().lines().collect(Collectors.joining());
			Oeuvre oeuvre = (new Gson().fromJson(body, Oeuvre.class));
			Statement st;
			int id = 0;
			try {
				st = connexionBD.getCnx().createStatement();
				ResultSet res = st.executeQuery("SELECT id FROM kartinabdd.oeuvres order by id desc limit 0,1");
				if (res.next()) {
					id = res.getInt("id");
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			PreparedStatement ps = connexionBD.getCnx().prepareStatement(
					"UPDATE kartinabdd.oeuvres set prix_de_base=?, id_artiste=" + id_artiste + ", titre_photo=?, nb_tirages=?, nb_tirages_restants=?, date_mise_en_vente=now() where id=" + id);

			ps.setDouble(1, oeuvre.getPrixDeBase());
			ps.setString(2, oeuvre.getTitrePhoto());
			ps.setInt(3, oeuvre.getNbTirages());
			ps.setInt(4, oeuvre.getNbTirages());
			ps.execute();
			ps.close();

			for (Theme theme : oeuvre.getThemes()) {
				PreparedStatement ps2 = connexionBD.getCnx()
						.prepareStatement("INSERT INTO kartinabdd.themes_oeuvres (id_theme, id_oeuvre) VALUES(?,?)");
				switch (theme) {
				case NOIR_ET_BLANC:
					ps2.setInt(1, 1);
					break;
				case ANIMAUX:
					ps2.setInt(1, 6);
					break;
				case MUSIQUE:
					ps2.setInt(1, 5);
					break;
				case NOURRITURE:
					ps2.setInt(1, 3);
					break;
				case SPORTS:
					ps2.setInt(1, 4);
					break;
				case VOYAGES:
					ps2.setInt(1, 2);
					break;
				default:
					break;
				}
				ps2.setInt(2, id);
				ps2.execute();
				ps2.close();
			}
			
			for (Format format : oeuvre.getFormats()) {
				PreparedStatement ps2 = connexionBD.getCnx()
						.prepareStatement("INSERT INTO kartinabdd.formats_oeuvres (id_format, id_oeuvre) VALUES(?,?)");
				switch (format) {
				case CLASSIQUE:
					ps2.setInt(1, 1);
					break;
				case GRAND:
					ps2.setInt(1, 2);
					break;
				case GEANT:
					ps2.setInt(1, 3);
					break;
				case COLLECTOR:
					ps2.setInt(1, 4);
					break;
				default:
					break;
				}
				ps2.setInt(2, id);
				ps2.execute();
				ps2.close();
			}

		} catch (

		SQLException e) {
			e.printStackTrace();
		}
	}

}
