package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Orientation;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererPhotos")
public class RecupererPhotos extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public RecupererPhotos() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			int nbPhotosParPage = Integer.parseInt(getServletContext().getInitParameter("nbPhotosParPage"));
			int numeroPage = Integer.parseInt(request.getParameter("numeroPage"));
			String orderBy = request.getParameter("orderBy").toString();
			String ascdesc = request.getParameter("ascdesc").toString();
			String derniers = request.getParameter("derniers").toString();
			String nouveautes = request.getParameter("nouveautes").toString();
			String theme = request.getParameter("theme").toString();
			int idtheme = 0;
			if (!theme.equals("false")) {
				idtheme = Integer.parseInt(theme);
			}
			String orientation = request.getParameter("orientation").toString();
			String format = request.getParameter("format").toString();
			String prix = request.getParameter("prix");
			
			Statement st = connexionBD.getCnx().createStatement();
			
            ResultSet res = st.executeQuery((nouveautes.equals("true") ? "select * from (" : "" ) + "SELECT oeuvres.id as id_oeuvre, id_orientation, prix_de_base, date_mise_en_vente, nb_tirages, nb_tirages_restants, titre_photo, lien_image, utilisateurs.id as id_artiste, nom, prenom from oeuvres inner join utilisateurs ON utilisateurs.id = oeuvres.id_artiste " + 
					(!theme.equals("false") ? "inner join themes_oeuvres on themes_oeuvres.id_oeuvre=oeuvres.id " : "") + 
					(!format.equals("false") ? "inner join formats_oeuvres on oeuvres.id = formats_oeuvres.id_oeuvre " : "") +
					"where nb_tirages_restants>0 " +
					(derniers.equals("true") ? "and nb_tirages_restants/nb_tirages<=0.1" : "") + 
					(derniers.equals("true") &&  !theme.equals("false") ? "and id_theme=" + idtheme + " " : (!theme.equals("false") ? "and id_theme=" + idtheme + " " : "")) +
					((derniers.equals("true") || !theme.equals("false")) && !orientation.equals("false") ? "and " + orientation + " " : (!orientation.equals("false") ? "and " + orientation + " " : "" )) + 
					((derniers.equals("true") || !theme.equals("false") || !orientation.equals("false"))  && !format.equals("false") ? "and " + format + " " : (!format.equals("false") ? "and " + format + " " : "" )) +
					((derniers.equals("true") || !theme.equals("false") || !orientation.equals("false")  || !format.equals("false")) && !prix.equals("false") ? "and " + prix + " " : (!prix.equals("false") ? "and " + prix + " " : "" )) +
					("group by oeuvres.id ") +
					(nouveautes.equals("true") ? "order by date_mise_en_vente desc, id_oeuvre desc " : "order by " + orderBy + " " + ascdesc + "") + " limit " + (numeroPage == 1 ? 0 : (numeroPage - 1) * nbPhotosParPage) + "," + nbPhotosParPage + (nouveautes.equals("true") ? ") as t order by " + orderBy + " " + ascdesc + ", id_oeuvre desc" : ""));
           
            List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
            while (res.next()) {
            	Artiste artiste = new Artiste();
            	Oeuvre oeuvre = new Oeuvre();
            	artiste.setNom(res.getString("nom"));
            	artiste.setPrenom(res.getString("prenom"));
            	artiste.setId(res.getInt("id_artiste"));
            	oeuvre.setArtiste(artiste);
            	oeuvre.setId(res.getInt("id_oeuvre"));
            	
            	switch(res.getInt("id_orientation")) {
				case 1:
					oeuvre.setOrientation(Orientation.CARRE);
					break;
				case 2:
					oeuvre.setOrientation(Orientation.PAYSAGE);
					break;
				case 3:
					oeuvre.setOrientation(Orientation.PANORAMIQUE);
					break;
				case 4:
					oeuvre.setOrientation(Orientation.PORTRAIT);
					break;
				default: System.out.println("Il manque l'orientation");
            	}
            	
            	oeuvre.setPrixDeBase(res.getDouble("prix_de_base"));
            	oeuvre.setTitrePhoto(res.getString("titre_photo"));
            	oeuvre.setLienImage(res.getString("lien_image"));
            	oeuvres.add(oeuvre);            	
            }
            
            String json = new Gson().toJson(oeuvres);
            response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(json);
			
            
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
