package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.classes.Commande;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/NouvelleCommande")
public class NouvelleCommande extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public NouvelleCommande() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		String body = request.getReader().lines().collect(Collectors.joining());
		Client client = new Gson().fromJson(body, Client.class);
		Commande c = new Gson().fromJson(body, Commande.class);

		try {
			ConnexionBD connexionBD = new ConnexionBD();

			PreparedStatement ps = connexionBD.getCnx().prepareStatement(
					"INSERT INTO kartinabdd.commandes (id_client, date_achat, id_statut, prix) VALUES (?,now(),1,?)");
			ps.setInt(1, client.getId());
			ps.setDouble(2, c.getPrix());
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
