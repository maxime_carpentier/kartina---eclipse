package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/ModifierStatutVendeur")
public class ModifierStatutVendeur extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ModifierStatutVendeur() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
        	ConnexionBD connexionBD = new ConnexionBD();  		
        	request.setCharacterEncoding("UTF-8");
    		String body = request.getReader().lines().collect(Collectors.joining());
    		Type collectionType = new TypeToken<List<Client>>(){}.getType();
    		@SuppressWarnings("unchecked")
			List<Client> clients = (List<Client>) (new Gson().fromJson(body, collectionType));
    		
    		for (Client client : clients) {
    			PreparedStatement ps = connexionBD.getCnx().prepareStatement("UPDATE kartinabdd.utilisateurs SET compteBloque=?, artiste=? where id=?");
    			ps.setBoolean(1, client.isCompteBloque());
    			ps.setBoolean(2, client.isArtiste());
    			ps.setInt(3, client.getId());
    			ps.execute();
    			ps.close();
    		}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
