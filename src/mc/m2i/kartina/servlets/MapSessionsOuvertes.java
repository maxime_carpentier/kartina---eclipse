package mc.m2i.kartina.servlets;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

@WebServlet("/MapSessionsOuvertes")
public class MapSessionsOuvertes extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static Map<Integer,HttpSession> mapSessions = new HashMap<Integer,HttpSession>();

    public Map<Integer, HttpSession> getMapSessions() {
		return mapSessions;
	}

	public void addMapSessions(int id, HttpSession session) {
		MapSessionsOuvertes.mapSessions.put(id, session);
	}
	
	public void removeMapSessions(int id) {
		MapSessionsOuvertes.mapSessions.remove(id);
	}

	public MapSessionsOuvertes() {
        super();
    }

}
