package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Orientation;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererVentesPasseesParArtiste")
public class RecupererVentesPasseesParArtiste extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RecupererVentesPasseesParArtiste() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		try {
			ConnexionBD conn = new ConnexionBD();
			Statement st = conn.getCnx().createStatement();
			ResultSet res = st.executeQuery(
					"select id_orientation, lien_image, titre_photo, date_mise_en_vente, date_fin_de_vente, nb_tirages, prix_de_base from oeuvres where nb_tirages_restants = 0 and id_artiste="
							+ id);
			List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
			while (res.next()) {
				Oeuvre oeuvre = new Oeuvre();
				switch (res.getInt("id_orientation")) {
				case 1:
					oeuvre.setOrientation(Orientation.CARRE);
					break;
				case 2:
					oeuvre.setOrientation(Orientation.PAYSAGE);
					break;
				case 3:
					oeuvre.setOrientation(Orientation.PANORAMIQUE);
					break;
				case 4:
					oeuvre.setOrientation(Orientation.PORTRAIT);
					break;
				default:
					System.out.println("Il manque l'orientation");
				}
				oeuvre.setLienImage(res.getString("lien_image"));
				oeuvre.setTitrePhoto(res.getString("titre_photo"));
				oeuvre.setDateMiseEnVente(res.getDate("date_mise_en_vente"));
				oeuvre.setNbTirages(res.getInt("nb_tirages"));
				oeuvre.setDateFinVente(res.getDate("date_fin_de_vente"));
				oeuvre.setPrixDeBase(res.getDouble("prix_de_base"));
				oeuvres.add(oeuvre);
			}
			String json = new Gson().toJson(oeuvres);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
