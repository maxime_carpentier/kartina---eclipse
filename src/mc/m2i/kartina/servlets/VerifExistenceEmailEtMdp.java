package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/VerifExistenceEmailEtMdp")
public class VerifExistenceEmailEtMdp extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public VerifExistenceEmailEtMdp() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionBD connexionBD = new ConnexionBD();
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			
			Client c = new Client();
			c.setMotDePasse(password);
			
			System.out.println("mot de passe : " + password + " / " + c.getMotDePasse());

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select * from authentification where email='" + email + "' AND mot_de_passe ='" + c.getMotDePasse() + "'");
			
			boolean exist = false;
			if (res.next()) {
				exist = true;
			}
			
			String json = new Gson().toJson(exist);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
