package mc.m2i.kartina.servlets;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/Test")
public class Test extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public int id_orientation = 0;
	public String lien_image = "";
	public int id = 0;
	
	public Test() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("bien recu");
		DataInputStream in = new DataInputStream(request.getInputStream());
		int formDataLength = request.getContentLength();
		byte[] dataBytes = new byte[formDataLength];
		int byteRead = 0;
		int totalBytesRead = 0;
		while (totalBytesRead < formDataLength) {
			byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
			totalBytesRead += byteRead;
		}
		
		ConnexionBD connexionBD = new ConnexionBD();
		Statement st;
		try {
			st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("SELECT id FROM kartinabdd.oeuvres order by id desc limit 0,1");
			if (res.next()) {
				id = res.getInt("id") + 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		lien_image = "photo" + id + ".jpg";
		File file = new File("C:\\Users\\Maxime\\Documents\\Workspace-J2EE\\kartina---eclipse\\WebContent\\photos\\photo" + id + ".jpg");
		//File file = new File("C:\\Users\\Formation\\eclipse-workspace-j2ee\\Kartina\\WebContent\\photos");
		if(!file.exists()){
			file.createNewFile();
		}
		InputStream input = new ByteArrayInputStream(dataBytes);
		BufferedImage bImageFromConvert = ImageIO.read(input);
		double div = ((double)(bImageFromConvert.getWidth()))/((double)(bImageFromConvert.getHeight()));
		if(div == 1) {
			id_orientation = 1;
		} else if (div < 1) {
			id_orientation = 4;
		} else if (div >= 3) {
			id_orientation = 3;
		} else {
			id_orientation = 2;
		}
		ImageIO.write(bImageFromConvert, "jpg", file);
		
		try {
			PreparedStatement ps2 = connexionBD.getCnx()
					.prepareStatement("INSERT INTO kartinabdd.oeuvres (id, lien_image, id_orientation) VALUES(?,?,?)");
			ps2.setInt(1, id);
			ps2.setString(2, lien_image);
			ps2.setInt(3, id_orientation);
			ps2.execute();
			ps2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
