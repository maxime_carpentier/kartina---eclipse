package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/VerifCompteBloqueConnexion")
public class VerifCompteBloqueConnexion extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public VerifCompteBloqueConnexion() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionBD connexionBD = new ConnexionBD();
			String email = request.getParameter("email");

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select compteBloque from utilisateurs where email='" + email + "'");
			
			boolean compteBloque = false;
			if (res.next()) {
				if (res.getBoolean("compteBloque")) {
					compteBloque = true;
				}
			}
			
			String json = new Gson().toJson(compteBloque);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
