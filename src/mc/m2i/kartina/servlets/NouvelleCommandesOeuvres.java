package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Cadre;
import mc.m2i.kartina.enumerations.Finition;
import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/NouvelleCommandesOeuvres")
public class NouvelleCommandesOeuvres extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public NouvelleCommandesOeuvres() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		String body = request.getReader().lines().collect(Collectors.joining());
		Type collectionType = new TypeToken<List<Oeuvre>>(){}.getType();
		@SuppressWarnings("unchecked")
		List<Oeuvre> oeuvres = (List<Oeuvre>) (new Gson().fromJson(body, collectionType));

		try {
			ConnexionBD connexionBD = new ConnexionBD();

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("SELECT id FROM kartinabdd.commandes order by id desc limit 0,1");
			
			int id_commande = 0;
			
			if (res.next()) {
				id_commande = res.getInt("id");
			}
			
			for (Oeuvre oeuvre : oeuvres) {
				PreparedStatement ps = connexionBD.getCnx().prepareStatement(
						"INSERT INTO kartinabdd.commandes_oeuvres (id_commande, id_oeuvre, id_cadre, id_finition, id_format, quantite, prix) VALUES (" + id_commande + ",?,?,?,?,?,?)");
				ps.setInt(1, oeuvre.getId());
				ps.setInt(2, Cadre.valueOf(oeuvre.getCadre().toString()).ordinal()+1);
				ps.setInt(3, Finition.valueOf(oeuvre.getFinition().toString()).ordinal()+1);
				ps.setInt(4, Format.valueOf(oeuvre.getFormatChoisi().toString()).ordinal()+1);
				ps.setInt(5, oeuvre.getQuantite());
				ps.setDouble(6, oeuvre.getPrixAchat());
				ps.execute();
				ps.close();
				PreparedStatement ps2 = connexionBD.getCnx().prepareStatement(
						"UPDATE kartinabdd.oeuvres SET nb_tirages_restants = nb_tirages_restants-1 WHERE id=?");
				ps2.setInt(1, oeuvre.getId());
				ps2.execute();
				ps2.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
