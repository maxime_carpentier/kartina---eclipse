package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Orientation;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererArtiste")
public class RecupererArtiste extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public RecupererArtiste() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("bjr")) {
                    System.out.println(cookie.getValue());
                }
            }
        }
        try {
        	ConnexionBD connexionBD = new ConnexionBD();
    		int id = Integer.parseInt(request.getParameter("id"));
    		
    		Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("SELECT nom, prenom, biographie FROM kartinabdd.utilisateurs where id=" + id );
			
			Artiste a = new Artiste();
			
			if (res.next()) {
				a.setNom(res.getString("nom"));
				a.setPrenom(res.getString("prenom"));
				a.setBiographie(res.getString("biographie"));
			}
			
			Statement st2 = connexionBD.getCnx().createStatement();
			ResultSet res2 = st2.executeQuery("SELECT id, id_orientation, titre_photo, lien_image, prix_de_base FROM kartinabdd.oeuvres where id_artiste=" + id );
			
			List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
			
			while (res2.next()) {
				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setPrixDeBase(res2.getDouble("prix_de_base"));
				oeuvre.setId(res2.getInt("id"));
				oeuvre.setTitrePhoto(res2.getString("titre_photo"));
				oeuvre.setLienImage(res2.getString("lien_image"));
				switch(res2.getInt("id_orientation")) {
            	case 1: oeuvre.setOrientation(Orientation.CARRE); break;
            	case 2: oeuvre.setOrientation(Orientation.PAYSAGE); break;
            	case 3: oeuvre.setOrientation(Orientation.PANORAMIQUE); break;
            	case 4: oeuvre.setOrientation(Orientation.PORTRAIT); break;
            	default: System.out.println("Il manque l'orientation");
				}
				oeuvres.add(oeuvre);
			}
			
			a.setOeuvres(oeuvres);
			
			String json = new Gson().toJson(a);
            response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(json);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
