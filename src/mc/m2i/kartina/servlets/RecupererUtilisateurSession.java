package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererUtilisateurSession")
public class RecupererUtilisateurSession extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RecupererUtilisateurSession() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			int id = Integer.parseInt(request.getParameter("id"));

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select * from utilisateurs where id=" + id);

			String json = "";
			if (res.next()) {
				Client client = new Client();
				client.setId(res.getInt("id"));
				client.setCivilite(res.getString("civilite"));
				client.setPrenom(res.getString("prenom"));
				client.setNom(res.getString("nom"));
				client.setTelephone(res.getString("telephone"));
				client.setEmail(res.getString("email"));
				Adresse adresse = new Adresse();
				adresse.setCodePostal(res.getString("codePostal"));
				adresse.setRue(res.getString("rue"));
				adresse.setVille(res.getString("ville"));
				adresse.setPays(res.getString("pays"));
				client.setAdresse(adresse);
				json = new Gson().toJson(client);
			}

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (

		SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
