package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mc.m2i.kartina.classes.Commande;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/ModifierStatutCommandes")
public class ModifierStatutCommandes extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    public ModifierStatutCommandes() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
        	ConnexionBD connexionBD = new ConnexionBD();  		
        	request.setCharacterEncoding("UTF-8");
    		String body = request.getReader().lines().collect(Collectors.joining());
    		Type collectionType = new TypeToken<List<Commande>>(){}.getType();
    		@SuppressWarnings("unchecked")
			List<Commande> commandes = (List<Commande>) (new Gson().fromJson(body, collectionType));
    		
    		for (Commande commande : commandes) {
    			PreparedStatement ps = connexionBD.getCnx().prepareStatement("UPDATE kartinabdd.commandes SET id_statut=? where id=?");
    			switch(commande.getStatut()) {
    			case EN_ATTENTE_DE_TRAITEMENT : ps.setInt(1, 1); break;
    			case EN_COURS_DE_PREPARATION : ps.setInt(1, 2); break;
    			case EN_COURS_D_ACHEMINEMENT: ps.setInt(1, 3); break;
    			case LIVREE : ps.setInt(1, 4); break;
    			default: break;
    			}
    			ps.setInt(2, commande.getId());
    			ps.execute();
    			ps.close();
    		}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
