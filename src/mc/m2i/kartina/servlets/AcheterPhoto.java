package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.enumerations.Orientation;

/**
 * Servlet implementation class AcheterPhoto
 */
@WebServlet("/AcheterPhoto")
public class AcheterPhoto extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ArrayList<Oeuvre> achatOeuvre = new ArrayList<>();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AcheterPhoto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		System.out.println("taille 2 : " + achatOeuvre.size());
		
		
		String json = new Gson().toJson(achatOeuvre);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String body = request.getReader().lines().collect(Collectors.joining());	
		
		System.out.println("body>>>>>>>" + body);
		//request.setAttribute("body", body);

		try
		{
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			Connection cnx = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris",
				"root", "admin");
			
			System.out.println("La connexion est " + (cnx.isClosed() ? "fermée" : "ouverte"));
			System.out.println("l'idphoto___ est " + body);
			String photoid = body;
			System.out.println("-----" + photoid + "-----");

			
			Statement st = cnx.createStatement();

			ResultSet res1 = st.executeQuery("select prenom, nom, biographie, utilisateurs.id as idU, oeuvres.id as idO, prix_de_base, titre_photo, lien_image, id_orientation, id_format " + 
											"from utilisateurs, oeuvres, formats_oeuvres " + 
											"where utilisateurs.id = oeuvres.id_artiste and oeuvres.id = id_oeuvre and oeuvres.id = "+ photoid);
			
			
				achatOeuvre.clear();
			
			
			while(res1.next())
			{
				Artiste artiste = new Artiste();
				artiste.setPrenom(res1.getString("prenom"));
				artiste.setNom(res1.getString("nom"));
				artiste.setBiographie(res1.getString("biographie"));
				artiste.setId(res1.getInt("idU"));
				
				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setId(res1.getInt("idO"));
				oeuvre.setPrixDeBase(res1.getFloat("prix_de_base"));
				oeuvre.setTitrePhoto(res1.getString("titre_photo"));
				oeuvre.setLienImage(res1.getString("lien_image"));
				LinkedList<Format> listFormat = new LinkedList<>();
				switch(res1.getInt("id_format"))
				{
				case 1:
					oeuvre.setFormatChoisi(Format.CLASSIQUE);
					break;
				case 2:
					oeuvre.setFormatChoisi(Format.GRAND);
					break;
				case 3:
					oeuvre.setFormatChoisi(Format.GEANT);
					break;
				case 4:
					oeuvre.setFormatChoisi(Format.COLLECTOR);
					break;
				default: System.out.println("Il manque le format");
				}


				switch(res1.getInt("id_orientation")) {
				case 1:
					oeuvre.setOrientation(Orientation.CARRE);
					break;
				case 2:
					oeuvre.setOrientation(Orientation.PAYSAGE);
					break;
				case 3:
					oeuvre.setOrientation(Orientation.PANORAMIQUE);
					break;
				case 4:
					oeuvre.setOrientation(Orientation.PORTRAIT);
					break;
				default: System.out.println("Il manque l'orientation");
            	}
				oeuvre.setArtiste(artiste);
								
				achatOeuvre.add(oeuvre);
				
				System.out.println("taille 1 : " + achatOeuvre.size());
			}
			
			
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		
		
		doGet(request, response);
	}

}
