package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/NbrePhotos")
public class NbrePhotos extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public NbrePhotos() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			String derniers = request.getParameter("derniers").toString();
			String nouveautes = request.getParameter("nouveautes").toString();
			String theme = request.getParameter("theme").toString();
			int idtheme = 0;
			if (!theme.equals("false")) {
				idtheme = Integer.parseInt(theme);
			}
			String orientation = request.getParameter("orientation");
			String format = request.getParameter("format");
			String prix = request.getParameter("prix");
			
			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select count(*) as nb_oeuvres from (SELECT oeuvres.id, id_orientation, prix_de_base, titre_photo, lien_image, utilisateurs.id as id_artiste, nom, prenom from oeuvres inner join utilisateurs ON utilisateurs.id = oeuvres.id_artiste " + 
							(!theme.equals("false") ? "inner join themes_oeuvres on themes_oeuvres.id_oeuvre=oeuvres.id " : "") + 
							(!format.equals("false") ? "inner join formats_oeuvres on oeuvres.id = formats_oeuvres.id_oeuvre " : "") +
							"where nb_tirages_restants>0 " +
							(derniers.equals("true") ? "and nb_tirages_restants/nb_tirages<=0.1 " : "") + 
							(derniers.equals("true") &&  !theme.equals("false") ? "and id_theme=" + idtheme + " " : (!theme.equals("false") ? "and id_theme=" + idtheme + " " : "")) +
							((derniers.equals("true") || !theme.equals("false")) && !orientation.equals("false") ? "and " + orientation + " " : (!orientation.equals("false") ? "and " + orientation + " " : "" )) + 
							((derniers.equals("true") || !theme.equals("false") || !orientation.equals("false"))  && !format.equals("false") ? "and " + format + " " : (!format.equals("false") ? "and " + format + " " : "" )) +
							((derniers.equals("true") || !theme.equals("false") || !orientation.equals("false")  || !format.equals("false")) && !prix.equals("false") ? "and " + prix + " " : (!prix.equals("false") ? "and " + prix + " " : "" )) +
							("group by oeuvres.id ") +		
							") as t");
							
			int nb_oeuvres = 0;
			if (res.next()) {
				nb_oeuvres = res.getInt("nb_oeuvres");
			}
			if (nouveautes.equals("true")) {
				nb_oeuvres = 24;
			}
			int nbPhotosParPage = Integer.parseInt(getServletContext().getInitParameter("nbPhotosParPage"));
			int nbPages = (nb_oeuvres%nbPhotosParPage==0 ? nb_oeuvres/nbPhotosParPage: nb_oeuvres/nbPhotosParPage+1);
			Map<String,Integer> map = new HashMap<String,Integer>();
			map.put("nb_oeuvres", nb_oeuvres);
			map.put("nbPages", nbPages);
			
			String json = new Gson().toJson(map);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
