package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.helpers.ConnexionBD;


@WebServlet("/RecupererFormatsParPhoto")
public class RecupererFormatsParPhoto extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public RecupererFormatsParPhoto() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionBD connexionBD = new ConnexionBD();
			int id = Integer.parseInt(request.getParameter("id_oeuvre"));
			
			Statement st = connexionBD.getCnx().createStatement();
            ResultSet res = st.executeQuery("SELECT id_format FROM kartinabdd.formats_oeuvres where id_oeuvre= " + id);
           
            List<Format> formats = new LinkedList<Format>();
            while (res.next()) {
            	switch(res.getInt("id_format")) {
				case 1:
					formats.add(Format.CLASSIQUE);
					break;
				case 2:
					formats.add(Format.GRAND);
					break;
				case 3:
					formats.add(Format.GEANT);
					break;
				case 4:
					formats.add(Format.COLLECTOR);
					break;
				default: System.out.println("Il manque le format");
            	}         	
            }
            
            

            String json = new Gson().toJson(formats);
            response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(json);
			
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
