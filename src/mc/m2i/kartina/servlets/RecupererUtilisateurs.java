package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererUtilisateurs")
public class RecupererUtilisateurs extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RecupererUtilisateurs() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			ConnexionBD connexionBD = new ConnexionBD();

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st
					.executeQuery("SELECT id, nom, prenom, admin, artiste, compteBloque FROM kartinabdd.utilisateurs");

			List<Client> utilisateurs = new LinkedList<Client>();
			while (res.next()) {
				if (!res.getBoolean("admin")) {
					Client client;
					if (res.getBoolean("artiste")) {
						client = new Artiste();
					} else {
						client = new Client();
					}
					client.setId(res.getInt("id"));
					client.setNom(res.getString("nom"));
					client.setPrenom(res.getString("prenom"));
					client.setCompteBloque(res.getBoolean("compteBloque"));
					utilisateurs.add(client);
				}
			}

			String json = new Gson().toJson(utilisateurs);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
