package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.classes.Commande;
import mc.m2i.kartina.enumerations.Statut;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererCommandes")
public class RecupererCommandes extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RecupererCommandes() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select commandes.id, prix, id_statut, date_achat, utilisateurs.nom, utilisateurs.prenom from commandes inner join utilisateurs on commandes.id_client = utilisateurs.id order by commandes.id");

			List<Commande> commandes = new LinkedList<Commande>();
			
			while (res.next()) {
				Commande commande = new Commande();
				commande.setId(res.getInt("id"));
				commande.setPrix(res.getDouble("prix"));
				commande.setDateAchat(res.getDate("date_achat"));
				switch(res.getInt("id_statut")) {
            	case 1: commande.setStatut(Statut.EN_ATTENTE_DE_TRAITEMENT); break;
            	case 2: commande.setStatut(Statut.EN_COURS_DE_PREPARATION); break;
            	case 3: commande.setStatut(Statut.EN_COURS_D_ACHEMINEMENT); break;
            	case 4: commande.setStatut(Statut.LIVREE); break;
            	default: System.out.println("Il manque le statut");
            	}
				Client client = new Client();
				client.setNom(res.getString("nom"));
				client.setPrenom(res.getString("prenom"));
				commande.setClient(client);
				commandes.add(commande);
			}

			String json = new Gson().toJson(commandes);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
