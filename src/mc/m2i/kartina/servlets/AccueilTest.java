package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Orientation;

/**
 * Servlet implementation class AccueilTest
 */
@WebServlet("/AccueilTest")
public class AccueilTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccueilTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());

			// PrintWriter out = response.getWriter();

			Connection cnx = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris",
					"root", "admin");
			System.out.println("La connexion est " + (cnx.isClosed() ? "fermée" : "ouverte"));

			LinkedList<Oeuvre> oeuvresAccueil = new LinkedList<>();

			Statement st = cnx.createStatement();

			ResultSet res1 = st.executeQuery("select * " + "from ("
					+ "select oeuvres.id, id_orientation, titre_photo, lien_image, date_mise_en_vente "
					+ "from oeuvres " + "where nb_tirages_restants>0 order by date_mise_en_vente desc, oeuvres.id desc " + "limit 24) " + "as req "
					+ "order by rand() limit 1;");

			while (res1.next()) {
				Oeuvre oeuvre = new Oeuvre();
				switch (res1.getInt("id_orientation")) {
				case 1:
					oeuvre.setOrientation(Orientation.CARRE);
					break;
				case 2:
					oeuvre.setOrientation(Orientation.PAYSAGE);
					break;
				case 3:
					oeuvre.setOrientation(Orientation.PANORAMIQUE);
					break;
				case 4:
					oeuvre.setOrientation(Orientation.PORTRAIT);
					break;
				default:
					System.out.println("Il manque l'orientation");
				}
				oeuvre.setId(res1.getInt("id"));
				oeuvre.setTitrePhoto(res1.getString("titre_photo"));
				oeuvre.setLienImage(res1.getString("lien_image"));

				oeuvresAccueil.add(oeuvre);
			}

			ResultSet res2 = st.executeQuery(
					"SELECT oeuvres.id as idO, prix_de_base, titre_photo, id_orientation, lien_image, nb_tirages - nb_tirages_restants as ventes, id_artiste, utilisateurs.id as idA, prenom, nom "
							+ "FROM oeuvres, utilisateurs " + "where id_artiste = utilisateurs.id and nb_tirages_restants>0 "
							+ "order by ventes desc " + "limit 6");

			while (res2.next()) {
				Artiste artiste = new Artiste();
				artiste.setId(res2.getInt("idA"));
				artiste.setPrenom(res2.getString("prenom"));
				artiste.setNom(res2.getString("nom"));

				Oeuvre oeuvre = new Oeuvre();
				switch(res2.getInt("id_orientation")) {
            	case 1: oeuvre.setOrientation(Orientation.CARRE); break;
            	case 2: oeuvre.setOrientation(Orientation.PAYSAGE); break;
            	case 3: oeuvre.setOrientation(Orientation.PANORAMIQUE); break;
            	case 4: oeuvre.setOrientation(Orientation.PORTRAIT); break;
            	default: System.out.println("Il manque l'orientation");
            	}
				oeuvre.setId(res2.getInt("idO"));
				oeuvre.setPrixDeBase(res2.getFloat("prix_de_base"));
				oeuvre.setTitrePhoto(res2.getString("titre_photo"));
				oeuvre.setLienImage(res2.getString("lien_image"));
				oeuvre.setArtiste(artiste);

				oeuvresAccueil.add(oeuvre);
			}

			ResultSet res3_1 = st.executeQuery(
					"SELECT oeuvres.id, titre_photo, lien_image, prenom, nom " + "FROM oeuvres, utilisateurs "
							+ "WHERE oeuvres.id IN(" + "SELECT id_oeuvre " + "FROM themes_oeuvres "
							+ "WHERE id_theme IN(" + "SELECT themes.id " + "FROM themes " + "WHERE theme = 'Voyages')) "
							+ "AND oeuvres.id_artiste = utilisateurs.id and nb_tirages_restants>0 " + "ORDER BY RAND() LIMIT 1");

			while (res3_1.next()) {
				Artiste artiste = new Artiste();
				artiste.setPrenom(res3_1.getString("prenom"));
				artiste.setNom(res3_1.getString("nom"));

				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setId(res3_1.getInt("id"));
				oeuvre.setTitrePhoto(res3_1.getString("titre_photo"));
				oeuvre.setLienImage(res3_1.getString("lien_image"));
				oeuvre.setArtiste(artiste);

				oeuvresAccueil.add(oeuvre);
			}

			ResultSet res3_2 = st.executeQuery("SELECT oeuvres.id, titre_photo, lien_image, prenom, nom "
					+ "FROM oeuvres, utilisateurs " + "WHERE oeuvres.id IN(" + "SELECT id_oeuvre "
					+ "FROM themes_oeuvres " + "WHERE id_theme IN(" + "SELECT themes.id " + "FROM themes "
					+ "WHERE theme = 'Noir et blanc')) " + "AND oeuvres.id_artiste = utilisateurs.id and nb_tirages_restants>0 "
					+ "ORDER BY RAND() LIMIT 1");

			while (res3_2.next()) {
				Artiste artiste = new Artiste();
				artiste.setPrenom(res3_2.getString("prenom"));
				artiste.setNom(res3_2.getString("nom"));

				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setId(res3_2.getInt("id"));
				oeuvre.setTitrePhoto(res3_2.getString("titre_photo"));
				oeuvre.setLienImage(res3_2.getString("lien_image"));
				oeuvre.setArtiste(artiste);

				oeuvresAccueil.add(oeuvre);
			}

			ResultSet res3_3 = st.executeQuery(
					"select oeuvres.id as idO, titre_photo, lien_image,id_artiste, utilisateurs.id as idA, prenom, nom "
							+ "from oeuvres, utilisateurs " + "where id_artiste = utilisateurs.id and nb_tirages_restants>0 "
							+ "order by rand() limit 1");

			while (res3_3.next()) {
				Artiste artiste = new Artiste();
				artiste.setId(res3_3.getInt("idA"));
				artiste.setPrenom(res3_3.getString("prenom"));
				artiste.setNom(res3_3.getString("nom"));

				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setId(res3_3.getInt("idO"));
				oeuvre.setTitrePhoto(res3_3.getString("titre_photo"));
				oeuvre.setLienImage(res3_3.getString("lien_image"));
				oeuvre.setArtiste(artiste);

				oeuvresAccueil.add(oeuvre);
			}

			ResultSet res3_4 = st.executeQuery(
					"select * from(" + "SELECT *, nb_tirages / nb_tirages_restants as ventes FROM oeuvres "
							+ "where nb_tirages / nb_tirages_restants >= 10 and nb_tirages_restants>0 " + "order by ventes) as req "
							+ "order by rand() limit 1");

			while (res3_4.next()) {
				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setId(res3_4.getInt("id"));
				oeuvre.setTitrePhoto(res3_4.getString("titre_photo"));
				oeuvre.setLienImage(res3_4.getString("lien_image"));

				oeuvresAccueil.add(oeuvre);

			}

			Map<String, LinkedList<Oeuvre>> map = new HashMap<String, LinkedList<Oeuvre>>();

			map.put("photo", oeuvresAccueil);

			String json = new Gson().toJson(oeuvresAccueil);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("accueil");

	}

}
