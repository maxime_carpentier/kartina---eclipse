package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererUtilisateurViaEmail")
public class RecupererUtilisateurViaEmail extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public RecupererUtilisateurViaEmail() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			ConnexionBD connexionBD = new ConnexionBD();
			String email = request.getParameter("email");

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select * from utilisateurs where email='" + email + "'");
			
			String json = "";
			if (res.next()) {
				Adresse adresse = new Adresse();
				adresse.setRue(res.getString("rue"));
				adresse.setCodePostal(res.getString("codePostal"));
				adresse.setVille(res.getString("ville"));
				adresse.setPays(res.getString("pays"));
				if(res.getBoolean("artiste")) {
					Artiste artiste = new Artiste();
					artiste.setBiographie(res.getString("biographie"));
					artiste.setId(res.getInt("id"));
					if (res.getBoolean("admin")) {
						artiste.setAdmin(true);
					} else {
						artiste.setAdmin(false);
					}
					artiste.setCivilite(res.getString("civilite"));
					artiste.setNom(res.getString("nom"));
					artiste.setPrenom(res.getString("prenom"));
					artiste.setNom(res.getString("nom"));
					artiste.setEmail(res.getString("email"));
					artiste.setTelephone(res.getString("telephone"));
					artiste.setAdresse(adresse);
					json = new Gson().toJson(artiste);
				} else {
					Client client = new Client();
					client.setId(res.getInt("id"));
					if (res.getBoolean("admin")) {
						client.setAdmin(true);
					} else {
						client.setAdmin(false);
					}
					client.setCivilite(res.getString("civilite"));
					client.setNom(res.getString("nom"));
					client.setPrenom(res.getString("prenom"));
					client.setNom(res.getString("nom"));
					client.setEmail(res.getString("email"));
					client.setTelephone(res.getString("telephone"));
					client.setAdresse(adresse);
					json = new Gson().toJson(client);
				}
			}
			
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
