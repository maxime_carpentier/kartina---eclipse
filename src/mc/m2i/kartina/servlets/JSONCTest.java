package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Client;

/**
 * Servlet implementation class JSONCTest
 */
@WebServlet("/JSONCTest")
public class JSONCTest extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JSONCTest()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		try
		{
			// Chargement du pilote en environnement Java EE
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			PrintWriter out = response.getWriter();

			/************************************************
			 * CONNECTION A LA BDD EN LOCAL !!!!!!!!
			 ***************************************************/
			Connection cnx = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris",
					"root", "admin");
			System.out.println("La connexion est " + (cnx.isClosed() ? "fermée" : "ouverte"));
			String body = request.getReader().lines().collect(Collectors.joining());
			System.out.println("body>>>>>>>" + body);

			Client c = new Gson().fromJson(body, Client.class);
			System.out.println("client.toString()>>>>>>>>>>>>>" + c.toString());
			// out.print("La connexion est " + (cnx.isClosed()?"fermée":"ouverte"));
			if (c.verifier(cnx))
			{
				System.out.println("client existant");
				if(!c.compteBloque(cnx))
				{
					System.out.println("compte valide");
				}
				else
				{
					System.out.println("compte bloqué");
				}


			} else
			{
				System.out.println("client non inscrit");
				// out.print("L'email est déjà utilisé");
			}
		}

		catch (IOException | SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
