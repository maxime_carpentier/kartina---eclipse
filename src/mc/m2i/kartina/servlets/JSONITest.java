package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.classes.Client;

@WebServlet("/JSONITest")
public class JSONITest extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public JSONITest()
	{
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		try
		{
			System.out.println("doget");
			//System.out.println("request.getattribut" + request.getAttribute("status"));

			String confirmation;
			if(request.getAttribute("status").toString().equals("true"))
			//if(true)
			{
				confirmation = "{\"status\": true}";
			}
			else
			{
				confirmation = "{\"status\": false}";
			}

			System.out.println(confirmation);
			
			 //String confirmation = "";
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");

			response.getWriter().append(confirmation);
			//response.getWriter().append(request.getAttribute("status").toString());
			//System.out.println("client ajouté à la bdd");
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		
		
		
		
		
		try
		{
			// Chargement du pilote en environnement Java EE

			System.out.println("bonjour dopost");
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			// PrintWriter out = response.getWriter();
			/************************************************
			 * CONNECTION A LA BDD EN LOCAL !!!!!!!!
			 ***************************************************/
			Connection cnx = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris",
					"root", "admin");
			System.out.println("La connexion est " + (cnx.isClosed() ? "fermée" : "ouverte"));
			String body = request.getReader().lines().collect(Collectors.joining());
			System.out.println("body>>>>>>>" + body);

			Client c = new Gson().fromJson(body, Client.class);
			Adresse a = new Gson().fromJson(body, Adresse.class);
			c.setAdresse(a);
			System.out.println("client inscrit" + c.getNom());
			System.out.println("rue du client : " + c.getAdresse().getRue());
			System.out.println(a.getRue());
			// System.out.println("isAdmin : " + );
			@SuppressWarnings("unused")
			String confirmation;

			if (!c.verifier(cnx))
			{
				c.ajouter(cnx);
				
				//confirmation = request.getParameter("{\"status\": true}");
				request.setAttribute("status", "true");
				// envoi JSON a faire
				//confirmation = "{\"status\": true}";
				// doGet(request, response, confirmation);

				//

				//
			} else
			{
				// // envoi JSON a faire
				//confirmation = "{\"status\": false}";
				// doGet(request, response, confirmation);
				request.setAttribute("status", "false");
				System.out.println("client déjà inscrit");
				// out.print("L'email est déjà utilisé");
			}
			doGet(request, response);

		}

		catch (IOException | SQLException | NoSuchAlgorithmException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
