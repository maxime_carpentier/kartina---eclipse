package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/InscrireUtilisateur")
public class InscrireUtilisateur extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public InscrireUtilisateur() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			request.setCharacterEncoding("UTF-8");
			String body = request.getReader().lines().collect(Collectors.joining());
			Client c = new Gson().fromJson(body, Client.class);
			Adresse a = new Gson().fromJson(body, Adresse.class);
			System.out.println(a.getRue());

			PreparedStatement ps = connexionBD.getCnx().prepareStatement(
					"INSERT INTO kartinabdd.utilisateurs (civilite, prenom, nom, telephone, rue, codePostal, ville, pays, compteBloque, admin, artiste, email) VALUES (?,?,?,?,?,?,?,?,false,false,false,?)");
			ps.setString(1, c.getCivilite());
			ps.setString(2, c.getPrenom());
			ps.setString(3, c.getNom());
			ps.setString(4, c.getTelephone());
			ps.setString(5, a.getRue());
			ps.setString(6, a.getCodePostal());
			ps.setString(7, a.getVille());
			ps.setString(8, a.getPays());
			ps.setString(9, c.getEmail());
			ps.execute();
			ps.close();

			PreparedStatement ps2 = connexionBD.getCnx()
					.prepareStatement("INSERT INTO kartinabdd.authentification (email, mot_de_passe) VALUES (?,?)");
			ps2.setString(1, c.getEmail());
			ps2.setString(2, c.getMotDePasse());
			ps2.execute();
			ps2.close();

			PreparedStatement ps3 = connexionBD.getCnx()
					.prepareStatement("INSERT INTO kartinabdd.authentification_utilisateur (id_utilisateur, id_authentification) VALUES (?,?)");
			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select id from utilisateurs where email='" + c.getEmail() + "'");
			if (res.next()) {
				System.out.println(res.getInt("id"));
				ps3.setInt(1, res.getInt("id"));
			}
			Statement st2 = connexionBD.getCnx().createStatement();
			ResultSet res2 = st2.executeQuery("select id from authentification where email='" + c.getEmail() + "'");
			if (res2.next()) {
				System.out.println(res2.getInt("id"));
				ps3.setInt(2, res2.getInt("id"));
			}
			ps3.execute();
			ps3.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
