package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Aide;
import mc.m2i.kartina.classes.EnvoiMailAide;

/**
 * Servlet implementation class ChargerClientAide
 */
@WebServlet("/EnvoiEmailClientAide")
public class EnvoiEmailClientAide extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EnvoiEmailClientAide() {
		super();
		System.out.println("je rentre dans prog 1");
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		System.out.println("je rentre dans prog 2");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		System.out.println("je rentre dans prog 3");
		request.setCharacterEncoding("UTF-8");
		String body = request.getReader().lines().collect(Collectors.joining());
		
		Aide a = new Gson().fromJson(body, Aide.class);		
				
		System.out.println("je rentre dans prog 4");
		EnvoiMailAide.envoi(a);
	}
		
	
		
}