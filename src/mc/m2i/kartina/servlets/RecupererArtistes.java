package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererArtistes")
public class RecupererArtistes extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
    public RecupererArtistes() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			String lettre = request.getParameter("lettre").toString();
			
			Statement st = connexionBD.getCnx().createStatement();
            ResultSet res = st.executeQuery("SELECT id, nom, prenom FROM kartinabdd.utilisateurs " + 
            								"where artiste=true and (nom like '"+ lettre.toUpperCase() + "%' or nom like'" + lettre.toLowerCase() + "%')");
           
            List<Artiste> artistes = new LinkedList<Artiste>();
            while (res.next()) {
            	Artiste artiste = new Artiste();
            	artiste.setNom(res.getString("nom"));
            	artiste.setPrenom(res.getString("prenom"));
            	artiste.setId(res.getInt("id"));
            	Statement st2 = connexionBD.getCnx().createStatement();
                ResultSet res2 = st2.executeQuery("select lien_image from oeuvres where id_artiste=" + artiste.getId() + " order by rand() limit 0,1");
            	if (res2.next()) {
            		List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
            		Oeuvre oeuvre = new Oeuvre();
            		oeuvre.setLienImage(res2.getString("lien_image"));
            		oeuvres.add(oeuvre);
            		artiste.setOeuvres(oeuvres);
            	}
            	artistes.add(artiste);            	
            }
            
            

            String json = new Gson().toJson(artistes);
            response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(json);
			
            
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
