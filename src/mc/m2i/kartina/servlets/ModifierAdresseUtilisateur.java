package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Adresse;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/ModifierAdresseUtilisateur")
public class ModifierAdresseUtilisateur extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ModifierAdresseUtilisateur() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		String body = request.getReader().lines().collect(Collectors.joining());
		Adresse a = new Gson().fromJson(body, Adresse.class);

		int id = Integer.parseInt(request.getParameter("id"));

		try {
			ConnexionBD connexionBD = new ConnexionBD();

			PreparedStatement ps = connexionBD.getCnx()
					.prepareStatement("UPDATE kartinabdd.utilisateurs SET rue=?, ville=?, codePostal=?, pays=? where id=" + id);
			ps.setString(1, a.getRue());
			ps.setString(2, a.getVille());
			ps.setString(3, a.getCodePostal());
			ps.setString(4, a.getPays());
			ps.execute();
			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
