package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import mc.m2i.kartina.classes.Client;

/**
 * Servlet implementation class ChargerClientAide
 */
@WebServlet("/ChargerClientAide")
public class ChargerClientAide extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChargerClientAide() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
	// TODO Auto-generated method stub
		
		// Connexion à MySQL
		
		Client nclient = new Client();
		try
        {
			System.out.println("chargement de la requette");
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris", "root", "admin");
            Statement st = cnx.createStatement();
            ResultSet res = st.executeQuery("select * from utilisateurs where email = 'aurelien.villette@hotmail.fr'");
            
            if (res.next()) {
            	
            	            	
            	nclient.setCivilite(res.getString("civilite"));
            	nclient.setNom(res.getString("nom"));
            	nclient.setPrenom(res.getString("prenom"));
            	nclient.setEmail(res.getString("email"));
            	nclient.setTelephone(res.getString("telephone"));
            	            	
            } else {
            	System.out.println("requette non aboutie");
            }
            
            res.close();
			st.close();
            cnx.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
	
		// création de l'instance client pour envoie des paramètres à component.html de Angular
	/*	Client nclient = new Client();
	 	nclient.setCivilite("Melle");
		nclient.setNom("Nom test dur");
		nclient.setPrenom("Prenom test dur");
		nclient.setEmail("email@testdur.com");
		nclient.setTelephone("test 0622115598");     */
			
		// envoie des paramètres à aide.component.ts de Angular
		String json = new Gson().toJson(nclient);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(json); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub			
	}
}
