package mc.m2i.kartina.servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import mc.m2i.kartina.classes.Artiste;
import mc.m2i.kartina.classes.Client;
import mc.m2i.kartina.classes.Commande;
import mc.m2i.kartina.classes.Oeuvre;
import mc.m2i.kartina.enumerations.Cadre;
import mc.m2i.kartina.enumerations.Finition;
import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.enumerations.Orientation;
import mc.m2i.kartina.enumerations.Statut;
import mc.m2i.kartina.helpers.ConnexionBD;

@WebServlet("/RecupererCommandeParId")
public class RecupererCommandeParId extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public RecupererCommandeParId() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			ConnexionBD connexionBD = new ConnexionBD();
			int idCommande = Integer.parseInt(request.getParameter("id"));

			Statement st = connexionBD.getCnx().createStatement();
			ResultSet res = st.executeQuery("select commandes.id as id_commande, commandes.prix as prix_total, id_statut, date_achat, \r\n" + 
					"	utilisateurs.nom, utilisateurs.prenom, \r\n" + 
					"    commandes_oeuvres.prix, commandes_oeuvres.id_cadre, commandes_oeuvres.id_finition, commandes_oeuvres.id_format, commandes_oeuvres.quantite,\r\n" + 
					"    oeuvres.id_orientation, oeuvres.id as id_oeuvre, oeuvres.titre_photo, oeuvres.lien_image \r\n" + 
					"from commandes\r\n" + 
					"inner join utilisateurs on utilisateurs.id = commandes.id_client\r\n" + 
					"inner join commandes_oeuvres on commandes.id = commandes_oeuvres.id_commande\r\n" + 
					"inner join oeuvres on oeuvres.id = commandes_oeuvres.id_oeuvre\r\n" + 
					"where commandes.id=" + idCommande);

			Commande commande = new Commande();
			List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
			while (res.next()) {	
				commande.setId(res.getInt("id_commande"));
				commande.setPrix(res.getDouble("prix_total"));
				commande.setDateAchat(res.getDate("date_achat"));
				switch(res.getInt("id_statut")) {
            	case 1: commande.setStatut(Statut.EN_ATTENTE_DE_TRAITEMENT); break;
            	case 2: commande.setStatut(Statut.EN_COURS_DE_PREPARATION); break;
            	case 3: commande.setStatut(Statut.EN_COURS_D_ACHEMINEMENT); break;
            	case 4: commande.setStatut(Statut.LIVREE); break;
            	default: System.out.println("Il manque le statut");
            	}
				Client client = new Client();
				client.setNom(res.getString("nom"));
				client.setPrenom(res.getString("prenom"));
				commande.setClient(client);
				Oeuvre oeuvre = new Oeuvre();
				oeuvre.setLienImage(res.getString("lien_image"));
				oeuvre.setTitrePhoto(res.getString("titre_photo"));
				switch(res.getInt("id_cadre")) {
            	case 1: oeuvre.setCadre(Cadre.SANS_ENCADREMENT); break;
            	case 2: oeuvre.setCadre(Cadre.NOIR_SATIN); break;
            	case 3: oeuvre.setCadre(Cadre.BLANC_SATIN); break;
            	case 4: oeuvre.setCadre(Cadre.NOYER); break;
            	case 5: oeuvre.setCadre(Cadre.CHÊNE); break;
            	case 6: oeuvre.setCadre(Cadre.NON_DISPONIBLE); break;
            	case 7: oeuvre.setCadre(Cadre.ALUMINIUM_NOIR); break;
            	case 8: oeuvre.setCadre(Cadre.BOIS_BLANC); break;
            	case 9: oeuvre.setCadre(Cadre.ACAJOU_MAT); break;
            	case 10: oeuvre.setCadre(Cadre.ALUMINIUM_BROSSE); break;
            	default: System.out.println("Il manque le cadre");
            	}
				switch(res.getInt("id_finition")) {
            	case 1: oeuvre.setFinition(Finition.ALUMINIUM); break;
            	case 2: oeuvre.setFinition(Finition.VERRE_ACRYLIQUE); break;
            	case 3: oeuvre.setFinition(Finition.PAPIER_PHOTO); break;
            	case 4: oeuvre.setFinition(Finition.BLACKOUT); break;
            	case 5: oeuvre.setFinition(Finition.ARTSHOT); break;
            	default: System.out.println("Il manque la finition");
            	}
				switch(res.getInt("id_format")) {
            	case 1: oeuvre.setFormatChoisi(Format.CLASSIQUE); break;
            	case 2: oeuvre.setFormatChoisi(Format.GRAND); break;
            	case 3: oeuvre.setFormatChoisi(Format.GEANT); break;
            	case 4: oeuvre.setFormatChoisi(Format.COLLECTOR); break;
            	default: System.out.println("Il manque le format");
            	}
				switch(res.getInt("id_orientation")) {
            	case 1: oeuvre.setOrientation(Orientation.CARRE); break;
            	case 2: oeuvre.setOrientation(Orientation.PAYSAGE); break;
            	case 3: oeuvre.setOrientation(Orientation.PANORAMIQUE); break;
            	case 4: oeuvre.setOrientation(Orientation.PORTRAIT); break;
            	default: System.out.println("Il manque l'orientation");
            	}
				oeuvre.setQuantite(res.getInt("quantite"));
				oeuvre.setPrixAchat(res.getDouble("prix"));
				oeuvre.setId(res.getInt("id_oeuvre"));
				Statement st2 = connexionBD.getCnx().createStatement();
				ResultSet res2 = st2.executeQuery("select nom, prenom from utilisateurs inner join oeuvres on utilisateurs.id=oeuvres.id_artiste where oeuvres.id=" + res.getInt("id_oeuvre"));
				Artiste artiste = new Artiste();
				while (res2.next()) {
					artiste.setNom(res2.getString("nom"));
					artiste.setPrenom(res2.getString("prenom"));
				}
				oeuvre.setArtiste(artiste);
				oeuvres.add(oeuvre);
			}
			commande.setOeuvres(oeuvres);
			
			String json = new Gson().toJson(commande);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
