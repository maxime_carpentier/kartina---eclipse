package mc.m2i.kartina.enumerations;

public enum Format {
	
	CLASSIQUE,
    GRAND,
    GEANT,
    COLLECTOR

}
