package mc.m2i.kartina.enumerations;

public enum Finition {
	
	ALUMINIUM,
    VERRE_ACRYLIQUE,
    PAPIER_PHOTO,
    BLACKOUT,
    ARTSHOT

}
