package mc.m2i.kartina.enumerations;

public enum Theme {
	
	NOIR_ET_BLANC,
    VOYAGES,
    NOURRITURE,
    SPORTS,
    MUSIQUE,
    ANIMAUX	

}
