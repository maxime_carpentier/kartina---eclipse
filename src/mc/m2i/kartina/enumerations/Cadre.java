package mc.m2i.kartina.enumerations;

public enum Cadre {
	
	SANS_ENCADREMENT,
    NOIR_SATIN,
    BLANC_SATIN,
    NOYER,
    CHÊNE,
    NON_DISPONIBLE,
    ALUMINIUM_NOIR,
    BOIS_BLANC,
    ACAJOU_MAT,
    ALUMINIUM_BROSSE

}
