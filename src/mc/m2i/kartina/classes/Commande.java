package mc.m2i.kartina.classes;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mc.m2i.kartina.enumerations.Statut;

public class Commande {
	
	private int id;
	private List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
	private Client client;
	private Date dateAchat;
	private Statut statut;
	private double prix;
	
	
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Oeuvre> getOeuvres() {
		return oeuvres;
	}
	public void setOeuvres(List<Oeuvre> oeuvres) {
		this.oeuvres = oeuvres;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Date getDateAchat() {
		return dateAchat;
	}
	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}
	public Statut getStatut() {
		return statut;
	}
	public void setStatut(Statut statut) {
		this.statut = statut;
	}
	
	

}
