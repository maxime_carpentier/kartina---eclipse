package mc.m2i.kartina.classes;

import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.MailerBuilder;

public class EnvoiMailAide {
	
	public static void envoi(Aide aide) {
		  System.out.println("je rentre dans prog 5");
		  
		  // Now set the actual message
		  String m = String.format("Vous avez reçu un message de la part de : %s %s %s%nNuméro de téléphone : %s%nVoici son message : %s",
				  aide.getUser().getCivilite(),
				  aide.getUser().getPrenom(),
				  aide.getUser().getNom(),
				  aide.getUser().getTelephone(),
				  aide.getMessage());
		  
//		  System.out.println(m);
		  
		  Mailer mailer = MailerBuilder
		          .withSMTPServer("localhost", 1025, "client@kartina.fr", "123456")
		          .buildMailer();
		  
		  Email email = EmailBuilder.startingBlank()
				    .from(aide.getUser().getEmail())
				    .to("client@kartina.fr")
				    .withSubject("Objet de la demande : " + aide.getSujet())
				    .withPlainText(m)
				    .buildEmail();

		mailer.sendMail(email);

		System.out.println("mail envoyé");
	   }
	}