package mc.m2i.kartina.classes;

import java.util.LinkedList;
import java.util.List;

public class Artiste extends Client {
	
	private List<Oeuvre> oeuvres = new LinkedList<Oeuvre>();
	private String biographie;
	
	
	public List<Oeuvre> getOeuvres() {
		return oeuvres;
	}
	public void setOeuvres(List<Oeuvre> oeuvres) {
		this.oeuvres = oeuvres;
	}
	public String getBiographie() {
		return biographie;
	}
	public void setBiographie(String biographie) {
		this.biographie = biographie;
	}
	
	public Artiste() {
		super.setArtiste(true);
	}
	

}
