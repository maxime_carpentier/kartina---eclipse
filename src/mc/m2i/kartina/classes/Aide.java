package mc.m2i.kartina.classes;

public class Aide {
	private Client user;
	private String sujet;
	private String message;

	/**
	 * @return the user
	 */
	public Client getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(Client user) {
		this.user = user;
	}

	public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Aide [client=" + user + ", sujet=" + sujet + ", message=" + message + "]";
	}

}