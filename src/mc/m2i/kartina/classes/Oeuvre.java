package mc.m2i.kartina.classes;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import mc.m2i.kartina.enumerations.Cadre;
import mc.m2i.kartina.enumerations.Finition;
import mc.m2i.kartina.enumerations.Format;
import mc.m2i.kartina.enumerations.Orientation;
import mc.m2i.kartina.enumerations.Theme;

public class Oeuvre {
	
	private int id;
	private List<Theme> themes = new LinkedList<Theme>();
	private List<Format> formats = new LinkedList<Format>();
	private Orientation orientation;
	private double prixDeBase;
	private double prixAchat;
	private Artiste artiste;
	private String titrePhoto;
	private String lienImage;
	private int nbTirages;
	private int nbTiragesRestants;
	private Date dateMiseEnVente;
	private Finition finition;
	private Cadre cadre;
	private int quantite;
	private Format formatChoisi;
	private Date dateFinVente;
	
	
	public Date getDateFinVente() {
		return dateFinVente;
	}
	public void setDateFinVente(Date dateFinVente) {
		this.dateFinVente = dateFinVente;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<Theme> getThemes() {
		return themes;
	}
	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}
	public List<Format> getFormats() {
		return formats;
	}
	public void setFormats(List<Format> formats) {
		this.formats = formats;
	}
	public Orientation getOrientation() {
		return orientation;
	}
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
	public double getPrixDeBase() {
		return prixDeBase;
	}
	public void setPrixDeBase(double prixDeBase) {
		this.prixDeBase = prixDeBase;
	}
	public Artiste getArtiste() {
		return artiste;
	}
	public void setArtiste(Artiste artiste) {
		this.artiste = artiste;
	}
	public String getTitrePhoto() {
		return titrePhoto;
	}
	public void setTitrePhoto(String titrePhoto) {
		this.titrePhoto = titrePhoto;
	}
	public String getLienImage() {
		return lienImage;
	}
	public void setLienImage(String lienImage) {
		this.lienImage = lienImage;
	}
	public int getNbTirages() {
		return nbTirages;
	}
	public void setNbTirages(int nbTirages) {
		this.nbTirages = nbTirages;
	}
	public int getNbTiragesRestants() {
		return nbTiragesRestants;
	}
	public void setNbTiragesRestants(int nbTiragesRestants) {
		this.nbTiragesRestants = nbTiragesRestants;
	}
	public Date getDateMiseEnVente() {
		return dateMiseEnVente;
	}
	public void setDateMiseEnVente(Date dateMiseEnVente) {
		this.dateMiseEnVente = dateMiseEnVente;
	}
	public Finition getFinition() {
		return finition;
	}
	public void setFinition(Finition finition) {
		this.finition = finition;
	}
	public Cadre getCadre() {
		return cadre;
	}
	public void setCadre(Cadre cadre) {
		this.cadre = cadre;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public Format getFormatChoisi() {
		return formatChoisi;
	}
	public void setFormatChoisi(Format formatChoisi) {
		this.formatChoisi = formatChoisi;
	}
	public double getPrixAchat() {
		return prixAchat;
	}
	public void setPrixAchat(double prixAchat) {
		this.prixAchat = prixAchat;
	}
	
	

}
