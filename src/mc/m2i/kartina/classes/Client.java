package mc.m2i.kartina.classes;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class Client
{

	private int id;
	private String civilite;
	private String prenom;
	private String nom;
	private String email;
	private String motDePasse;
	private String telephone;
	private Adresse adresse;
	private List<Commande> commandes = new LinkedList<Commande>();
	private boolean compteBloque;
	private boolean admin;
	private boolean artiste = false;

	private Statement instruction;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getCivilite()
	{
		return civilite;
	}

	public void setCivilite(String civilite)
	{
		this.civilite = civilite;
	}

	public String getPrenom()
	{
		return prenom;
	}

	public void setPrenom(String prenom)
	{
		this.prenom = prenom;
	}

	public String getNom()
	{
		return nom;
	}

	public void setNom(String nom)
	{
		this.nom = nom;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getMotDePasse() throws NoSuchAlgorithmException
	{
		byte[] bytesMDP = motDePasse.getBytes();		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] thedigest = md.digest(bytesMDP);		
		BigInteger bigInt = new BigInteger(1,thedigest);
		String hashtext = bigInt.toString(16);
		/*while(hashtext.length() < 32 )
		{
			hashtext = "0"+hashtext;
		}*/
		System.out.println(hashtext);
		
		return hashtext;
	}

	public void setMotDePasse(String motDePasse)
	{
		this.motDePasse = motDePasse;
	}

	public String getTelephone()
	{
		return telephone;
	}

	public void setTelephone(String telephone)
	{
		this.telephone = telephone;
	}

	public Adresse getAdresse()
	{
		return adresse;
	}

	public void setAdresse(Adresse adresse)
	{
		this.adresse = adresse;
	}

	public List<Commande> getCommandes()
	{
		return commandes;
	}

	public void setCommandes(List<Commande> commandes)
	{
		this.commandes = commandes;
	}

	public boolean isCompteBloque()
	{
		return compteBloque;
	}

	public void setCompteBloque(boolean compteBloque)
	{
		this.compteBloque = compteBloque;
	}

	public boolean isAdmin()
	{
		return admin;
	}

	public void setAdmin(boolean admin)
	{
		this.admin = admin;
	}

	public boolean isArtiste()
	{
		return artiste;
	}

	public void setArtiste(boolean artiste)
	{
		this.artiste = artiste;
	}

	public boolean verifier(Connection conn) throws SQLException
	{
		String requete = "SELECT * FROM UTILISATEURS WHERE email = '" + getEmail() + "'";
		instruction = conn.createStatement();
		ResultSet res = instruction.executeQuery(requete);

		if (res.next())
			return true;
		return false;
	}

	public boolean compteBloque(Connection conn) throws SQLException
	{

		String requete = "SELECT * FROM UTILISATEURS WHERE email = '" + getEmail() + "'";
		instruction = conn.createStatement();
		ResultSet res = instruction.executeQuery(requete);
		/*
		 * if(res.getInt(compteBloque) == 0) return false; return true;
		 */
		if (res.next())
		{
			System.out.println("compte bloqué : " + res.getString("compteBloque"));
			if (res.getString("compteBloque").equals("0"))
				return false;
			return true;
		}
		return false;
	}

	public void ajouter(Connection conn) throws SQLException, NoSuchAlgorithmException
	{
		String requete = "INSERT INTO UTILISATEURS VALUES (default, '" + getCivilite() + "', '" + getPrenom() + "', '"
				+ getNom() + "', '" + getEmail() + "', '" + getTelephone() + "', '" + getAdresse().getRue() + "', '"
				+ getAdresse().getCodePostal() + "', '" + getAdresse().getVille() + "', '" + getAdresse().getPays()
				+ "', '0', default, default, default)";

		String requete2 = "INSERT INTO AUTHENTIFICATION VALUES (default, '" + getEmail() + "', '" + getMotDePasse()
				+ "')";

		String requete3 = "INSERT INTO authentification_utilisateur (id_utilisateur, id_authentification) "
				+ "	SELECT utilisateurs.id , authentification.id " + "	FROM utilisateurs, authentification "
				+ "	WHERE utilisateurs.email='" + getEmail() + "' AND authentification.email='" + getEmail() + "'";

		instruction = conn.createStatement();
		System.out.println(requete);
		System.out.println("requete 1");
		instruction.executeUpdate(requete);
		System.out.println("requete 2");
		instruction.executeUpdate(requete2);
		System.out.println("requete 3");
		instruction.executeUpdate(requete3);

		// System.out.println(instruction.executeUpdate(requete));
	}

}
