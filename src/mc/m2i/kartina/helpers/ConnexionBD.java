package mc.m2i.kartina.helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionBD {
	
	private Connection cnx;
	
	public ConnexionBD() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			this.cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/kartinabdd?useLegacyDatetimeCode=false&serverTimezone=Europe/Paris", "root", "admin");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public Connection getCnx() {
		return cnx;
	}

	public void setCnx(Connection cnx) {
		this.cnx = cnx;
	}
	

}
